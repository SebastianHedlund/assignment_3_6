import guitar.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Guitar> myGuitarCollection = new ArrayList<Guitar>();

        ElectricGuitar myFirstGuitar = new ElectricGuitar("Squier", "Some random beginner model", 2005, 3);
        AcousticGuitar myAcousticGuitar = new AcousticGuitar("Cort", "Jade6 TWB", 2008);
        ElectricGuitar myFavoriteGuitar = new ElectricGuitar("Gibson", "SG Standard Cream Wite", 2011, 2);
        ElectroAcousticGuitar myElectroAcousticGuitar = new ElectroAcousticGuitar("Fender", "Nimbus2000", 2021, 2);
        Amplifier myAmp = new Amplifier("Marshall","2525C", 20, 2022);

        myGuitarCollection.add(myFirstGuitar);
        myGuitarCollection.add(myAcousticGuitar);
        myGuitarCollection.add(myFavoriteGuitar);

        myAcousticGuitar.makeSound();
        System.out.println();

        myAmp.makeSound(); //No sound (turned off)
        myAmp.setSwitchedOn(true);
        myAmp.makeSound(); //No sound since volume is set to 0
        System.out.println();

        myAmp.setCurrentVolume(70);
        myAmp.makeSound(); //Static sound since no connected Guitar
        System.out.println();

        myAmp.connectGuitar(myFavoriteGuitar);
        myAmp.makeSound(); //Electric Guitar sounds
        System.out.println();

        myAmp.disconnectGuitar();
        myAmp.makeSound(); //Static noises (no guitar connected)
        System.out.println();

        myAmp.connectGuitar(myAcousticGuitar); //Exception message, not an electric guitar
        System.out.println();

        myAmp.connectGuitar(myElectroAcousticGuitar);
        myAmp.makeSound(); //ElectroAcousticGuitar, new type of sound
        System.out.println();

        myAmp.connectGuitar(myFavoriteGuitar); //Exception message, already a connected guitar
        System.out.println();

        myAmp.makeSound(); //Still ElectroAcoustic sound
        myAmp.setSwitchedOn(false);

    }
}