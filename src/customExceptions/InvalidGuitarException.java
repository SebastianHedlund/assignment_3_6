package customExceptions;

public class InvalidGuitarException extends Exception{
    public InvalidGuitarException() {}
    public InvalidGuitarException(String message) {
        super(message);
    }
}
