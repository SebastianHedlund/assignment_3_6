package customExceptions;

public class AlreadyConnectedGuitarException extends Exception {

    public AlreadyConnectedGuitarException() {}
    public AlreadyConnectedGuitarException(String message) {
        super(message);
    }
}
