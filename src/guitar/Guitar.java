package guitar;

public abstract class Guitar implements Playable {
    private String brand;
    private String model;
    private int manufacturingYear;
    protected boolean isElectric;

    public Guitar(String brand, String model, int manufacturingYear) {
        this.brand = brand;
        this.model = model;
        this.manufacturingYear = manufacturingYear;
    }

    //getters and Setters
    public boolean getIsElectric() {
        return isElectric;
    }

}
