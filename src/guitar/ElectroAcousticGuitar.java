package guitar;

public class ElectroAcousticGuitar extends Guitar{
    private int numberOfPickUps;

    public ElectroAcousticGuitar(String brand, String model, int manufacturingYear, int numberOfPickUps) {
        super(brand, model, manufacturingYear);
        this.numberOfPickUps = numberOfPickUps;
        isElectric = true;
    }

    @Override
    public void makeSound() {
        System.out.println("Waaw-tring-ling-weey");
    }

}
