package guitar;

public interface Playable {
    void makeSound();
}
