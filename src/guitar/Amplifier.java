package guitar;

import customExceptions.AlreadyConnectedGuitarException;
import customExceptions.InvalidGuitarException;

public class Amplifier implements Playable {
    private String brand;
    private String model;
    private int wattage;
    private int manufacturingYear;
    private int currentVolume;
    private boolean switchedOn;
    private Guitar connectedGuitar;

    public Amplifier(String brand, String model, int wattage, int manufacturingYear, int currentVolume, boolean switchedOn, Guitar connectedGuitar) {
        this.brand = brand;
        this.model = model;
        this.wattage = wattage;
        this.manufacturingYear = manufacturingYear;
        this.currentVolume = currentVolume;
        this.switchedOn = switchedOn;
        this.connectedGuitar = connectedGuitar;
    }

    public Amplifier(String brand, String model, int wattage, int manufacturingYear) {
        this.brand = brand;
        this.model = model;
        this.wattage = wattage;
        this.manufacturingYear = manufacturingYear;
        currentVolume = 0;
        switchedOn = false;
    }

    @Override
    public void makeSound() {
        if(!switchedOn){
            return;
        }
        if(currentVolume == 0){
            System.out.println("Current volume: " + currentVolume + "dB.");
            return;
        }
        if(connectedGuitar == null){
            System.out.println("STATIC NOISES");
        }
        if(connectedGuitar != null){
            connectedGuitar.makeSound();
        }
        System.out.println("Current volume: " + currentVolume + "dB.");
    }

    public void connectGuitar(Guitar connectedGuitar){
        try{
            if(this.connectedGuitar != null)
                throw new AlreadyConnectedGuitarException("Can't connect guitar, another guitar is already connected.");
            if(!connectedGuitar.getIsElectric())
                throw new InvalidGuitarException("Received object is not a form of Electric Guitar.");

            this.connectedGuitar = connectedGuitar;

        }catch (Exception e){
            System.out.println(e);
        }

    }
    public void disconnectGuitar(){
        this.connectedGuitar = null;
    }

    //Getters & Setters
    public void setSwitchedOn(boolean switchedOn) {
        this.switchedOn = switchedOn;
    }

    public int getCurrentVolume() {
        return currentVolume;
    }

    public void setCurrentVolume(int currentVolume) {
        this.currentVolume = currentVolume;
    }

}

