package guitar;

public class ElectricGuitar extends Guitar{
    private int numberOfPickUps;

    public ElectricGuitar(String brand, String model, int manufacturingYear, int numberOfPickUps) {
        super(brand, model, manufacturingYear);
        this.numberOfPickUps = numberOfPickUps;
        isElectric = true;
    }

    @Override
    public void makeSound() {
        System.out.println("Waaw-wee-waa-weey");
    }

}
