package guitar;

public class AcousticGuitar extends Guitar{
    public AcousticGuitar(String brand, String model, int manufacturingYear) {
        super(brand, model, manufacturingYear);
    }

    @Override
    public void makeSound() {
        System.out.println("Tring-aring-ring");
    }
}
